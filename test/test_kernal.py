import numpy as np
from mpi4py.MPI import COMM_WORLD as comm
import sys
sys.path.append('..')

from lib.tool import memory_report
from lib.gce.gridtmp import Grid 
from lib.gce.const import Const
from lib.gce.out import Out
from lib.gce import space

dtype=np.complex128
# Form data to work on.
shape = (100,100,100)
space.initialize_space(shape)

xc=dtype((1 + 1j) * np.ones(shape))
x = Grid(xc,x_overlap=1)

for i in range(50000):
    x.synchronize()
    comm.Barrier()
    memory_report(str(i)+'-'+str(comm.rank)+'-')
