import numpy as np
from mpi4py.MPI import COMM_WORLD as comm
from pycuda import driver as drv
import sys
sys.path.append('..')

from lib.tool import memory_report
from lib.gce import space
from lib.gce.grid import Grid
from tmp import get_cpu_raw
dtype=np.complex128

shape=(4,4,4)
space.initialize_space(shape)
data = np.random.randn(*shape).astype(dtype)
cpu_data = np.empty_like(data)
comm.Allreduce(data, cpu_data)

g = Grid(cpu_data, x_overlap=1)
g1=g._get_raw()
print "g1=",g1
g0=g.get()
print "g0",g0
# Overwrite entire grid
cpu_raw_bad = get_cpu_raw(cpu_data, 1)
cpu_raw_bad[:1,:,:] += 1 # Mess up padding areas.
cpu_raw_bad[-1:,:,:] += 1
drv.memcpy_htod(g.data.ptr, cpu_raw_bad)

# Prove that the data is not synchronized at this time.
cpu_raw = get_cpu_raw(cpu_data, 1)
gd = g._get_raw()
print "g2=",gd

g.synchronize() # Synchronize the overlapping data.

# Make sure that the overlap data is accurate.
gd = g._get_raw()
print "g3=",gd
comm.Barrier() # Wait for other mpi nodes to finish.

