from lib.gce import space
from mpi4py.MPI import COMM_WORLD as comm
import numpy as np

def get_cpu_raw(cpu_data, k):
    # Make sure overlapped data is accurate as well.
    xr = space.get_space_info()['x_range']
    if comm.Get_rank() == 0:
        pad_back = cpu_data[-k:,:,:]
    else:
        pad_back = cpu_data[xr[0]-k:xr[0],:,:]

    if comm.Get_rank() == comm.Get_size() - 1:
        pad_front = cpu_data[:k,:,:]
    else:
        pad_front = cpu_data[xr[1]:xr[1]+k,:,:]

    return np.concatenate((pad_back, cpu_data[xr[0]:xr[1],:,:], \
                                pad_front), axis=0)
