import h5py
import numpy as np
from mpi4py.MPI import COMM_WORLD as comm
import time, sys, argparse
from math import pi

from lib.tool import write_results
from lib.tool import PML
from lib.tool import interp
from lib.tool import gradient_check
from lib import casimir
from lib.gce.grid import Grid
from lib.gce import verbose
from lib.gce.space import initialize_space
from lib.solver import Solver
from lib.filter import Filter
from lib.shapeopt import ellipsoid2dof_2d

sys.path.append('/home/weiliang/nlopt-2.5.0/lib/python2.7/site-packages/')
import nlopt

dtype=np.complex128
#------------- passing argument---------------#
parser = argparse.ArgumentParser()
# solvers
parser.add_argument('-name', action="store", type=str, default='test')
parser.add_argument('-maxit', action="store", type=int, default=100000)
parser.add_argument('-tol', action="store", type=float, default=1e-6)
parser.add_argument('-verbose', action="store", type=int, default=1)
parser.add_argument('-solverbase', action="store", type=int, default=100)
parser.add_argument('-init', action="store", type=int, default=10)
parser.add_argument('-init_type', action="store", type=str, default='zero')
parser.add_argument('-outputfield', action="store", type=int, default=0)
# filter
parser.add_argument('-bproj', action="store", type=float, default=0.0)
parser.add_argument('-R', action="store", type=int, default=0)
parser.add_argument('-alpha', action="store", type=float, default=1.0)
parser.add_argument('-rep', action="store", type=int, default=1)
# Job
parser.add_argument('-Job', action="store", type=int, default=1)
parser.add_argument('-Radius', action="store", type=float, default=1)
parser.add_argument('-alg', action="store", type=int, default=24)
parser.add_argument('-outputbase', action="store", type=int, default=2)
parser.add_argument('-export_in_object', action="store", type=int, default=1)
parser.add_argument('-maxeval', action="store", type=int, default=5000)
# temporal
parser.add_argument('-ifreq_start', action="store", type=float, default=0.01)
parser.add_argument('-ifreq_end', action="store", type=float, default=1.0)
parser.add_argument('-ifreq_N', action="store", type=int, default=10)
parser.add_argument('-Qabs', action="store", type=float, default=1e16)
parser.add_argument('-dispersion', action="store", type=float, default=-1)
# geometry
parser.add_argument('-stypexy', action="store", type=float, default=1.)
parser.add_argument('-stypez', action="store", type=float, default=1.)
parser.add_argument('-Nx', action="store", type=int, default=75)
parser.add_argument('-Ny', action="store", type=int, default=75)
parser.add_argument('-Nz', action="store", type=int, default=70)

parser.add_argument('-Mx', action="store", type=int, default=25)
parser.add_argument('-My', action="store", type=int, default=25)
parser.add_argument('-Mz', action="store", type=int, default=20)
parser.add_argument('-Mzslab', action="store", type=int, default=1)

parser.add_argument('-Mx0', action="store", type=int, default=0)
parser.add_argument('-My0', action="store", type=int, default=0)
parser.add_argument('-Mz0', action="store", type=int, default=0)

parser.add_argument('-Npmlx', action="store", type=int, default=10)
parser.add_argument('-Npmly', action="store", type=int, default=10)
parser.add_argument('-Npmlz', action="store", type=int, default=10)

parser.add_argument('-hx', action="store", type=float, default=0.02)
parser.add_argument('-hy', action="store", type=float, default=0.02)
parser.add_argument('-hz', action="store", type=float, default=0.02)

# dielectric
parser.add_argument('-epsbkg', action="store", type=float, default=1.0)
parser.add_argument('-epsdiff', action="store", type=float, default=11.0)
parser.add_argument('-epsfile', action="store", type=str, default='init.txt')
parser.add_argument('-epstype', action="store", type=str, default='vac')

# current source
parser.add_argument('-cx', action="store", type=int, default=37)
parser.add_argument('-cy', action="store", type=int, default=37)
parser.add_argument('-cz', action="store", type=int, default=35)
parser.add_argument('-alphax', action="store", type=float, default=1.0)
parser.add_argument('-alphay', action="store", type=float, default=1.0)
parser.add_argument('-alphaz', action="store", type=float, default=1.0)

r, unknown = parser.parse_known_args(sys.argv[1:])
if comm.rank==0:
    for arg in vars(r):
        print arg," is ",getattr(r,arg)
        
verbose.v=r.verbose
verbose.solverbase=r.solverbase
verbose.filename=r.name
verbose.outputbase=r.outputbase
verbose.export_in_object=r.export_in_object
verbose.init=r.init
verbose.init_type=r.init_type
verbose.outputfield=r.outputfield
# ----------assembling parameters------------#
omega = -1j*dtype(1.*2*pi)
shape = (r.Nx,r.Ny,r.Nz)

m=4.0
R=16.0
Npml = (r.Npmlx,r.Npmly,r.Npmlz)
hxyz = (r.hx,r.hy,r.hz)
# pml
pmlx=PML(r.Npmlx,omega,r.Nx,r.hx,m=m,R=R)
pmly=PML(r.Npmly,omega,r.Ny,r.hy,m=m,R=R)
pmlz=PML(r.Npmlz,omega,r.Nz,r.hz,m=m,R=R)

pml_p=[pmlx.sp*r.hx,pmly.sp*r.hy,pmlz.sp*r.hz]
pml_d=[pmlx.sd*r.hx,pmly.sd*r.hy,pmlz.sd*r.hz]

#interp matrix
if r.Mzslab == 1:
    ndof=(r.Mx/r.rep)*(r.My/r.rep)
else:
    ndof=(r.Mx/r.rep)*(r.My/r.rep)*(r.Mz/r.rep)
    
A = interp(r.Mx,r.My,r.Mz,r.Mzslab,r.Nx,r.Ny,r.Nz,r.Mx0,r.My0,r.Mz0,r.rep)

if r.epstype == 'one':
    dof=np.ones(ndof).astype(np.float)
elif r.epstype == 'rand':
    dof=np.random.random(ndof).astype(np.float)
elif r.epstype == 'file':
    f = open(r.epsfile, 'r')
    dof = np.loadtxt(f)
elif r.epstype == 'vac':
    dof=np.zeros(ndof).astype(np.float)
elif r.epstype == 'vacrand':
    dof=1e-2*np.random.random(ndof).astype(np.float)
elif r.epstype == 'hole' or r.epstype == 'holerand':
    x0=np.linspace(1, r.Mx/r.rep, r.Mx/r.rep)*r.hx*r.rep
    y0=np.linspace(1, r.My/r.rep, r.My/r.rep)*r.hy*r.rep
    z0=np.linspace(1, r.Mz/r.rep, r.Mz/r.rep)*r.hz*r.rep
    x,y,z = np.meshgrid(x0,y0,z0,indexing = 'ij')
    dof = np.zeros_like(x)
    dof[z>=r.Mz*r.hz-r.hz*2] = 1.
    
    isin = (x-0.5*r.Mx*r.hx)**2+(y-0.5*r.My*r.hy)**2<=r.Radius**2
    dof[isin] = 0.

    if r.epstype == 'holerand':
        dof = dof.flatten()
        dof = dof + 1e-2*np.random.random(ndof).astype(np.float)
        dof[dof>1.]=1.
    
if r.Mzslab == 1:
    dof = dof.reshape(r.Mx/r.rep,r.My/r.rep)
else:
    dof = dof.reshape(r.Mx/r.rep,r.My/r.rep,r.Mz/r.rep)    
dof = comm.bcast(dof)

x=[None] * 3
epsBkg=[None]
if comm.rank == 0:
    print "The size of the problem is",shape
    # epsilon profile
    epsBkg = np.ones(shape)*r.epsbkg

    ep1 = np.copy(epsBkg)
    A.matA(dof,ep1,r.epsdiff)

    # f=h5py.File('epsF.h5','w')
    # f.create_dataset('data',data=ep1)
    # f.close()

    # initial solution
    x=dtype([np.zeros(shape),np.zeros(shape),np.zeros(shape)])
    
#-------opertors----------#
#initiliaze GPU
initialize_space(shape)
# solver
solver1 = Solver.build_pml(shape, r.tol, r.maxit, Npml, hxyz, omega,m=m,R=R)

FT = Filter(r.Mx/r.rep,r.My/r.rep,r.bproj,r.R,r.alpha)
#-------solve-----------#
ifreq_list = np.linspace(r.ifreq_start,r.ifreq_end,r.ifreq_N)
alphaxyz = [r.alphax,r.alphay,r.alphaz]
grad=np.zeros(ndof)
if r.Qabs>1e10:
    r.Qabs=np.inf

if r.stypexy>0.:
    stypexy = r.stypexy
else:
    stypexy = None

if r.stypez>0.:
    stypez = r.stypez
else:
    stypez = None
    
fun_old=casimir.forceZintegrate(ifreq_list,solver1,A,epsBkg,r.epsdiff,x,alphaxyz,r.cx,r.cy,r.cz,Dmethod='center',step=1,stypexy=stypexy,stypez=stypez,Qabs=r.Qabs,dispersion=r.dispersion)
fun_nlopt = FT.new_object(fun_old)

if r.Job is 0:
    grad=np.array([])
    val,val_list=fun_nlopt(dof.flatten(),grad)
    print val,val_list

if r.Job is -2:
    grad=np.array([])
    dof = ellipsoid2dof_2d([r.hx*r.Mx/2,r.hy*r.My/2,r.Radius,r.Radius],1,hxyz,[r.Mx,r.My,r.Mz],hole=1,xsym=0,ysym=0)
    f=h5py.File('epsF.h5','w')
    f.create_dataset('data',data=np.reshape(dof,(r.Mx,r.My)))
    f.close()    
    val,val_list=fun_nlopt(dof.flatten(),grad)
    print val,val_list    
    
elif r.Job is -1:
    gradient_check(fun_nlopt, dof.flatten(), grad, index=1275, Ntotal=20, start=0.0, dx=1e-2)
    
elif r.Job is 1:
    lb=np.ones(ndof)*0.0
    ub=np.ones(ndof)*1.0
    
    opt = nlopt.opt(r.alg, ndof)
    opt.set_lower_bounds(lb)
    opt.set_upper_bounds(ub)
    
    opt.set_ftol_rel(1e-7)
    opt.set_maxeval(r.maxeval)

    opt.set_max_objective(fun_nlopt)
    
    x = opt.optimize(dof.flatten())
    minf = opt.last_optimum_value()

