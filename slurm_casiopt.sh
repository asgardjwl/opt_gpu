#!/bin/bash
#SBATCH --job-name=casimir
#SBATCH --output=test.txt
#SBATCH --gres=gpu:1
#SBATCH -N 1
#SBATCH --ntasks-per-node=1
#SBATCH --time=01:00:00
#SBATCH --mem=14000
#SBATCH --error=test.err
##SBATCH --mail-type=begin
##SBATCH --mail-type=end
#SBATCH --mail-user=wljin@stanford.edu

module load anaconda/5.3.0 openmpi/gcc/2.1.0/64 cudatoolkit/8.0
#module load anaconda/5.3.0 openmpi/cuda-8.0/gcc/2.1.0/64 cudatoolkit/9.0

prop=casimiropt.py
np=1

Qabs=1e16
hx=0.025
hy=$hx
hz=$hx
rep=2

d=0.5
gap=0.5
gap_dipole=1
mx=2
my=2
mz=2

epsdiff=100
epstype='rand'
epsfile='c.txt'
alphax=-1.0
alphay=-1.0
alphaz=1.0

fstart=0.05
fend=0.6
fN=3

bproj=0.0
alpha=2.
R=0

Nx=$(echo "($mx+2*$gap)/$hx" | bc);
Ny=$(echo "($my+2*$gap)/$hy" | bc);
Nz=$(echo "($mz+$d+$gap+$gap_dipole)/$hz" | bc);

Mx=$(echo "$mx/$hx" | bc);
My=$(echo "$my/$hy" | bc);
Mz=$(echo "$mz/$hz" | bc);

Mx0=$((($Nx-$Mx)/2))
My0=$((($Ny-$My)/2))
Mz0=$(echo "$gap/$hz" | bc);
Mzslab=1

Npmlx=10
Npmly=10
Npmlz=10

epsbkg=1
cx=$(($Nx/2))
cy=$(($Ny/2))
cz=$(echo "($gap+$mz+$d)/$hz" | bc);

fpre='casimir_z_Mx$Mx-My$My-Mz$Mz-rep$rep-slab$Mzslab-hx$hx-chi$epsdiff-fS$fstart-fe$fend-fN$fN-b$bproj-R$R-al$alpha-eps$epstype-Q$Qabs'
name=`hostname`
ID=$$
outputfile=$name-$fpre.$ID-out

# solver
maxit=100000
tol=1e-6
verbose=1
init=5
init_type='rand'
solverbase=5000
outputbase=2

eval "mpiexec -n $np python $prop -Nx $Nx -Ny $Ny -Nz $Nz -Mx $Mx -My $My -Mz $Mz -Mx0 $Mx0 -My0 $My0 -Mz0 $Mz0 -Mzslab $Mzslab -hx $hx -hy $hy -hz $hz -Npmlx $Npmlx -Npmly $Npmly -Npmlz $Npmlz -epsbkg $epsbkg -epsdiff $epsdiff -epstype $epstype -epsfile $epsfile -cx $cx -cy $cy -cz $cz -maxit $maxit -tol $tol -verbose $verbose -solverbase $solverbase -init $init -init_type $init_type -ifreq_start $fstart -ifreq_end $fend -ifreq_N $fN -alphax $alphax -alphay $alphay -alphaz $alphaz -rep $rep -bproj $bproj -alpha $alpha -R $R -outputbase $outputbase -name $fpre -Qabs $Qabs >$outputfile"
