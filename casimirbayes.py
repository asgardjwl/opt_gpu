import bayesopt as bo
import h5py
import numpy as np
from mpi4py.MPI import COMM_WORLD as comm
import time, sys, argparse
from math import pi

from lib.tool import write_results
from lib.tool import PML
from lib.tool import interp
from lib import casimir
from lib.gce.grid import Grid
from lib.gce import verbose
from lib.gce.space import initialize_space
from lib.solver import Solver
from lib.shapeopt import ellipsoid2dof, ellipsoid2dof_2d

sys.path.append('/home/weiliang/nlopt-2.5.0/lib/python2.7/site-packages/')
import nlopt

dtype=np.complex128
#------------- passing argument---------------#
parser = argparse.ArgumentParser()
# solvers
parser.add_argument('-name', action="store", type=str, default='test')
parser.add_argument('-maxit', action="store", type=int, default=100000)
parser.add_argument('-tol', action="store", type=float, default=1e-6)
parser.add_argument('-verbose', action="store", type=int, default=1)
parser.add_argument('-solverbase', action="store", type=int, default=1000)
parser.add_argument('-init', action="store", type=int, default=10)
parser.add_argument('-init_type', action="store", type=str, default='rand')
parser.add_argument('-outputfield', action="store", type=int, default=0)
# Job
parser.add_argument('-Job', action="store", type=int, default=1)
parser.add_argument('-outputbase', action="store", type=int, default=2)
parser.add_argument('-export_in_object', action="store", type=int, default=1)
parser.add_argument('-maxeval', action="store", type=int, default=500)
parser.add_argument('-relearn', action="store", type=int, default=10)
parser.add_argument('-inner', action="store", type=int, default=20)
parser.add_argument('-sample', action="store", type=int, default=10)
parser.add_argument('-hole', action="store", type=int, default=1)
# temporal
parser.add_argument('-ifreq_start', action="store", type=float, default=0.05)
parser.add_argument('-ifreq_end', action="store", type=float, default=1.0)
parser.add_argument('-ifreq_N', action="store", type=int, default=1)
parser.add_argument('-Qabs', action="store", type=float, default=1e16)
parser.add_argument('-dispersion', action="store", type=float, default=-1)
# DOF
parser.add_argument('-Ntotal', action="store", type=int, default=5)
parser.add_argument('-Rmin', action="store", type=float, default=0.1)
parser.add_argument('-Rmax', action="store", type=float, default=3.)
parser.add_argument('-centeroff', action="store", type=float, default=0.5)
# geometry
parser.add_argument('-stypexy', action="store", type=float, default=-1)
parser.add_argument('-stypez', action="store", type=float, default=-1)
parser.add_argument('-Nx', action="store", type=int, default=75)
parser.add_argument('-Ny', action="store", type=int, default=75)
parser.add_argument('-Nz', action="store", type=int, default=70)

parser.add_argument('-Mx', action="store", type=int, default=25)
parser.add_argument('-My', action="store", type=int, default=25)
parser.add_argument('-Mz', action="store", type=int, default=20)
parser.add_argument('-xsym', action="store", type=int, default=0)
parser.add_argument('-ysym', action="store", type=int, default=0)
parser.add_argument('-zsym', action="store", type=int, default=0)

parser.add_argument('-Mx0', action="store", type=int, default=0)
parser.add_argument('-My0', action="store", type=int, default=0)
parser.add_argument('-Mz0', action="store", type=int, default=0)

parser.add_argument('-Npmlx', action="store", type=int, default=10)
parser.add_argument('-Npmly', action="store", type=int, default=10)
parser.add_argument('-Npmlz', action="store", type=int, default=10)

parser.add_argument('-hx', action="store", type=float, default=0.02)
parser.add_argument('-hy', action="store", type=float, default=0.02)
parser.add_argument('-hz', action="store", type=float, default=0.02)

# dielectric
parser.add_argument('-epsbkg', action="store", type=float, default=1.0)
parser.add_argument('-epsdiff', action="store", type=float, default=11.0)
# current source
parser.add_argument('-cx', action="store", type=int, default=37)
parser.add_argument('-cy', action="store", type=int, default=37)
parser.add_argument('-cz', action="store", type=int, default=35)
parser.add_argument('-alphax', action="store", type=float, default=1.0)
parser.add_argument('-alphay', action="store", type=float, default=1.0)
parser.add_argument('-alphaz', action="store", type=float, default=1.0)

r, unknown = parser.parse_known_args(sys.argv[1:])
if comm.rank==0:
    for arg in vars(r):
        print arg," is ",getattr(r,arg)
        
verbose.v=r.verbose
verbose.solverbase=r.solverbase
verbose.filename=r.name
verbose.outputbase=r.outputbase
verbose.export_in_object=r.export_in_object
verbose.init=r.init
verbose.init_type=r.init_type
verbose.outputfield=r.outputfield
# ----------assembling parameters------------#
omega = -1j*dtype(1.*2*pi)
shape = (r.Nx,r.Ny,r.Nz)

m=4.0
R=16.0
Npml = (r.Npmlx,r.Npmly,r.Npmlz)
hxyz = (r.hx,r.hy,r.hz)
MxyzL = (r.Mx/(r.xsym+1),r.My/(r.ysym+1),r.Mz/(r.zsym+1))
# pml
pmlx=PML(r.Npmlx,omega,r.Nx,r.hx,m=m,R=R)
pmly=PML(r.Npmly,omega,r.Ny,r.hy,m=m,R=R)
pmlz=PML(r.Npmlz,omega,r.Nz,r.hz,m=m,R=R)

pml_p=[pmlx.sp*r.hx,pmly.sp*r.hy,pmlz.sp*r.hz]
pml_d=[pmlx.sd*r.hx,pmly.sd*r.hy,pmlz.sd*r.hz]

x=[None] * 3
epsBkg=[None]
if comm.rank == 0:
    print "The size of the problem is",shape
    # epsilon profile
    epsBkg = np.ones(shape)*r.epsbkg

    # initial solution
    x=dtype([np.zeros(shape),np.zeros(shape),np.zeros(shape)])
    
#-------opertors----------#
#initiliaze GPU
initialize_space(shape)
# solver
solver1 = Solver.build_pml(shape, r.tol, r.maxit, Npml, hxyz, omega,m=m,R=R)
#-------solve-----------#
ifreq_list = np.linspace(r.ifreq_start,r.ifreq_end,r.ifreq_N)
alphaxyz = [r.alphax,r.alphay,r.alphaz]

if r.Qabs>1e10:
    r.Qabs=np.inf

if r.stypexy>0.:
    stypexy = r.stypexy
else:
    stypexy = None

if r.stypez>0.:
    stypez = r.stypez
else:
    stypez = None

#interp matrix
rep = 1
if r.Job == 1:
    # 3D ellipsoidal
    Mzslab = 0
    A = interp(r.Mx,r.My,r.Mz,Mzslab,r.Nx,r.Ny,r.Nz,r.Mx0,r.My0,r.Mz0,rep)

    ndof=r.Ntotal*6
    xmin = -r.centeroff
    xmax = r.Mx/(1+r.xsym)*r.hx+r.centeroff
    ymin = -r.centeroff
    ymax = r.My/(1+r.ysym)*r.hy+r.centeroff
    zmin = -r.centeroff
    zmax = r.Mz/(1+r.zsym)*r.hz+r.centeroff

    lower_bound = np.zeros(ndof)
    upper_bound = np.zeros(ndof)
    for i in range(r.Ntotal):
        lower_bound[6*i] = xmin
        upper_bound[6*i] = xmax
        lower_bound[6*i+1] = ymin
        upper_bound[6*i+1] = ymax
        lower_bound[6*i+2] = zmin
        upper_bound[6*i+2] = zmax
        lower_bound[6*i+3] = r.Rmin
        upper_bound[6*i+3] = r.Rmax
        lower_bound[6*i+4] = r.Rmin
        upper_bound[6*i+4] = r.Rmax
        lower_bound[6*i+5] = r.Rmin
        upper_bound[6*i+5] = r.Rmax

if r.Job == 2:
    # plate-with-hole: only one variable, radius
    Mzslab = 1
    A = interp(r.Mx,r.My,r.Mz,Mzslab,r.Nx,r.Ny,r.Nz,r.Mx0,r.My0,r.Mz0,rep)

    ndof=1

    lower_bound = np.array([r.hx])
    upper_bound = np.array([r.Rmax])

fun=casimir.forceZintegrate(ifreq_list,solver1,A,epsBkg,r.epsdiff,x,alphaxyz,r.cx,r.cy,r.cz,Dmethod='center',step=1,stypexy=stypexy,stypez=stypez,Qabs=r.Qabs,dispersion=r.dispersion)

def my_fun(dof):
    dof = comm.bcast(dof)
    grad=np.array([])
    if r.Job == 1:
        e_grid = ellipsoid2dof(dof,r.Ntotal,hxyz,MxyzL,hole=r.hole,xsym=r.xsym,ysym=r.ysym,zsym=r.zsym)
    elif r.Job ==2:
        print dof
        dofnew = np.array([r.Mx/2,r.My/2,dof[0],dof[0]])
        e_grid = ellipsoid2dof_2d(dofnew,r.Ntotal,hxyz,MxyzL,hole=r.hole,xsym=r.xsym,ysym=r.ysym)
    val,val_list = fun(e_grid,grad)
    return -val
    
params = {} 
params['n_iterations'] = r.maxit
params['n_iter_relearn'] =r.relearn
params['n_inner_iterations']=r.inner
params['n_init_samples'] = r.sample
#params['crit_name'] = "cSum(cEI,cDistance)"
#params['crit_params'] = [1, 0.5]
#params['kernel_name'] = "kMaternISO3"
params['noise'] = 1e-10
params['epsilon']=0 #greedy

y_out, x_out, error = bo.optimize(my_fun,ndof,lower_bound,upper_bound,params)
