import h5py
import numpy as np
from mpi4py.MPI import COMM_WORLD as comm
from math import pi
import time, argparse, sys

from lib.tool import write_results,memory_report, PML, initialize
from lib.gce.grid import Grid
from lib.gce import verbose
from lib.gce.space import initialize_space
from lib.solver import Solver
from lib.rsvd import Inner, rsvd_singular, vec_product, vec_conj, rsvd
dtype=np.complex128

#------------- passing argument---------------#
parser = argparse.ArgumentParser()
# solvers
parser.add_argument('-filename', action="store", type=str, default='test')
parser.add_argument('-maxit', action="store", type=int, default=70000)
parser.add_argument('-tol', action="store", type=float, default=1e-6)
parser.add_argument('-verbose', action="store", type=int, default=1)
parser.add_argument('-solverbase', action="store", type=int, default=1000)
parser.add_argument('-init_type', action="store", type=str, default='zero')
# other
parser.add_argument('-is_real', action="store", type=int, default=1)
parser.add_argument('-is_geometry', action="store", type=float, default=1.0)
parser.add_argument('-svd_maxit', action="store", type=int, default=50)
parser.add_argument('-svd_tol', action="store", type=float, default=1e-2)
parser.add_argument('-test_PML', action="store", type=int, default=0)
# temporal
parser.add_argument('-freq', action="store", type=float, default=1.0)
parser.add_argument('-lam0', action="store", type=float, default=1.0)
parser.add_argument('-lam1', action="store", type=float, default=10.0)
parser.add_argument('-lam_num', action="store", type=int, default=10)
# geometry
parser.add_argument('-Nx', action="store", type=int, default=200)
parser.add_argument('-Ny', action="store", type=int, default=200)
parser.add_argument('-Nz', action="store", type=int, default=1)

parser.add_argument('-Mx1', action="store", type=int, default=100)
parser.add_argument('-My1', action="store", type=int, default=100)
parser.add_argument('-Mz1', action="store", type=int, default=0)
parser.add_argument('-dx1', action="store", type=int, default=2)
parser.add_argument('-dy1', action="store", type=int, default=2)
parser.add_argument('-dz1', action="store", type=int, default=1)

parser.add_argument('-Mx2', action="store", type=int, default=100)
parser.add_argument('-My2', action="store", type=int, default=104)
parser.add_argument('-Mz2', action="store", type=int, default=0)
parser.add_argument('-dx2', action="store", type=int, default=2)
parser.add_argument('-dy2', action="store", type=int, default=2)
parser.add_argument('-dz2', action="store", type=int, default=1)

parser.add_argument('-Npmlx', action="store", type=int, default=10)
parser.add_argument('-Npmly', action="store", type=int, default=10)
parser.add_argument('-Npmlz', action="store", type=int, default=0)

parser.add_argument('-hx', action="store", type=float, default=0.02)
parser.add_argument('-hy', action="store", type=float, default=0.02)
parser.add_argument('-hz', action="store", type=float, default=0.02)

r, unknown = parser.parse_known_args(sys.argv[1:])
if comm.rank==0:
    for arg in vars(r):
        print arg," is ",getattr(r,arg)
        
shape = (r.Nx,r.Ny,r.Nz)
omega = dtype(r.freq*2*pi)

pmlx=PML(r.Npmlx,omega,r.Nx,r.hx)
pmly=PML(r.Npmly,omega,r.Ny,r.hy)
pmlz=PML(r.Npmlz,omega,r.Nz,r.hz)
pml_p=[pmlx.sp*r.hx,pmly.sp*r.hy,pmlz.sp*r.hz]
pml_d=[pmlx.sd*r.hx,pmly.sd*r.hy,pmlz.sd*r.hz]

verbose.v=r.verbose
verbose.solverbase=r.solverbase
verbose.init_type=r.init_type

#initiliaze GPU
initialize_space(shape)
solver = Solver(shape, r.tol, r.maxit, pml_p, pml_d, omega)
init_type=r.init_type

region1=np.zeros(shape)
region1[r.Mx1:r.Mx1+r.dx1,r.My1:r.My1+r.dy1,r.Mz1:r.Mz1+r.dz1]=1
region2=np.zeros(shape)
region2[r.Mx2:r.Mx2+r.dx2,r.My2:r.My2+r.dy2,r.Mz2:r.Mz2+r.dz2]=1

def test_PML(x):
    y=[None]*3
    xinit=[None]*3
    if comm.rank == 0:
        y=[dtype(np.copy(x[i])*region1) for i in range(3)]
        y=solver.pre_cond(y)
        initialize(xinit, 0, 2, init_type, shape, dtype)
        
    xg = [Grid(dtype(f), x_overlap=1) for f in xinit]
    xg, err, success = solver.Multisolve(y, xg)

    field=[E.get() for E in xg]
    if comm.rank==0:
        field=solver.post_cond(field)
        write_results('t.h5',field)
       
def funreal(fun0):
    #real(f) x = (f+f')x/2 = (f(x) +f(x')')/2
    def fun(x):
        y1 = fun0(x)
        
        xc = [None]*3
        y = [None]*3
        if comm.rank == 0:
            xc = [np.conj(f) for f in x]
        y2=fun0(xc)

        if comm.rank == 0:
            y = [(y1[i] + np.conj(y2[i]))/2 for i in range(3)]

        return y
    return fun
    
def funImG(region):
    def fun(x):
        y=[None]*3
        xinit=[None]*3
        # G x
        if comm.rank == 0:
            y=[dtype(np.copy(x[i])*region) for i in range(3)]
            y=solver.pre_cond(y)
            initialize(xinit, 0, 2, init_type, shape, dtype)
        
        xg = [Grid(dtype(f), x_overlap=1) for f in xinit]
        xg, err, success = solver.Multisolve(y, xg)

        field=[E.get() for E in xg]
        if comm.rank==0:
            field=solver.post_cond(field)
            field=[field[i]*region for i in range(3)]

        # G* x = (G x*)*
        if comm.rank == 0:
            y=[dtype(np.conj(x[i])*region) for i in range(3)]
            y=solver.pre_cond(y)
            initialize(xinit, 0, 2, init_type, shape, dtype)
        
        xg = [Grid(dtype(f), x_overlap=1) for f in xinit]
        xg, err, success = solver.Multisolve(y, xg)

        y=[E.get() for E in xg]
        if comm.rank==0:
            y=solver.post_cond(y)
            y=[np.conj(y[i])*region for i in range(3)]
            # Im G x = (G x-G*x)/2i
            y=[(field[i]-y[i])/2j for i in range(3)]
        
        return y
    return fun

def funG21(x):
    y=[None]*3
    xinit=[None]*3
    if comm.rank == 0:
        y=[dtype(np.copy(x[i])*region1) for i in range(3)]
        y=solver.pre_cond(y)
        initialize(xinit, 0, 2, init_type, shape, dtype)
        
    xg = [Grid(dtype(f), x_overlap=1) for f in xinit]
    xg, err, success = solver.Multisolve(y, xg)

    field=[E.get() for E in xg]
    if comm.rank==0:
        field=solver.post_cond(field)
        y=[field[i]*region2 for i in range(3)]
        
    return y
    
def funG12(x):
    y=[None]*3
    xinit=[None]*3
    if comm.rank == 0:
        y=[dtype(np.copy(x[i])*region2) for i in range(3)]
        y=solver.pre_cond(y)
        initialize(xinit, 0, 2, init_type, shape, dtype)
        
    xg = [Grid(dtype(f), x_overlap=1) for f in xinit]
    xg, err, success = solver.Multisolve(y, xg)

    field=[E.get() for E in xg]
    if comm.rank==0:
        field=solver.post_cond(field)
        y=[field[i]*region1 for i in range(3)]
        
    return y

def funG21G12(x):
    y=[None]*3
    xinit=[None]*3
    # G12
    if comm.rank == 0:
        y=[dtype(np.copy(x[i])*region2) for i in range(3)]
        y=solver.pre_cond(y)
        initialize(xinit, 0, 2, init_type, shape, dtype)
        
    xg = [Grid(dtype(np.copy(f)), x_overlap=1) for f in xinit]
    xg, err, success = solver.Multisolve(y, xg)

    field=[E.get() for E in xg]
    if comm.rank==0:
        field=solver.post_cond(field)
        y=[field[i]*region1 for i in range(3)]

        y=solver.pre_cond(y)
        
    #G21
    xg = [Grid(dtype(f), x_overlap=1) for f in xinit]
    xg, err, success = solver.Multisolve(y, xg)
    field=[E.get() for E in xg]
    if comm.rank==0:
        field=solver.post_cond(field)
        y=[field[i]*region2 for i in range(3)]    
        
    return y    

def funG21G12_dagger(x):
    y=[None]*3
    if comm.rank == 0:
        y=[np.conj(f) for f in x]
    y2=funG21G12(y)
    if comm.rank == 0:
        y=[np.conj(f) for f in y2]
    return y

def funG21_dagger(x):
    y=[None]*3
    if comm.rank == 0:
        y=[np.conj(f) for f in x]
    y2=funG12(y)
    if comm.rank == 0:
        y=[np.conj(f) for f in y2]
    return y

def test_funG21():
    xt = [None]*3
    xt2 = [None]*3
    if comm.rank == 0:
        xt = [dtype(np.zeros(shape)) for i in range(3)]
        xt2 = [dtype(np.zeros(shape)) for i in range(3)]
        xt[2][r.Mx1,r.My1,r.Mz1]=1.0
        xt2[2][r.Mx1,r.My1,r.Mz1+r.dz1-1]=1.0

    y = funG21(xt)
    y2 = funG21(xt2)

    if comm.rank == 0:
        write_results('y.h5',y)
        write_results('y2.h5',y2)
    
if r.test_PML == 1:    
    xt=[None]*3
    if comm.rank == 0:
        xt = [np.random.random(shape) for k in range(3)]
    test_PML(xt)
    
if r.is_real == 1:
    s0 = rsvd_singular(funG21, funG21_dagger, r.svd_tol, r.svd_maxit, shape)
    s0=np.real(s0*(omega**2))
    if comm.rank == 0:
        np.savetxt(r.filename+'txt', s0)
        print s0
        
if r.is_real == 0:
    test_funG21()
    
    lam = np.logspace(r.lam0, r.lam1, r.lam_num)
    lam2 = lam ** 2
    #G21real = funreal(funG21)
    #dG21real = funreal(funG21_dagger)
    #U0,s0,V0 = rsvd(G21real, dG21real, r.svd_tol, r.svd_maxit, shape)    
    U0,s0,V0 = rsvd(funG21, funG21_dagger, r.svd_tol, r.svd_maxit, shape)
    s0=s0*(omega**2)
    if comm.rank == 0:
        part1 = sum(s0**2)
        print 'part1= ',part1
        np.save('s0',s0)
        np.save('v0',[U0[0],V0[0]])
        
    #G2112real = funreal(funG21G12)
    #dG2112real = funreal(funG21G12_dagger)
    #U,s,V = rsvd(G2112real, dG2112real, r.svd_tol, r.svd_maxit, shape)        
    U,s,V = rsvd(funG21G12, funG21G12_dagger, r.svd_tol, r.svd_maxit, shape)
    s=s*(omega**2)**2

    if comm.rank == 0:
        np.save('s',s)
        np.save('v',[U[0],V[0]])
        
        #related to part 2
        A1=vec_product(V,U0)
        A2=vec_product(U0,V)
        A3=vec_product(V,U)
        #related to part 3
        B1=vec_product(U0,U)

        val=[]
        for scale in lam2:
            A = scale*np.dot(np.diag(s),A1)
            A = np.dot(A,np.diag(s0**2))
            A = np.dot(A,A2)
            A = scale*np.dot(A,np.diag(s))
            inv = np.linalg.inv(np.eye(len(s))+scale*np.dot(np.diag(s),A3))
            A=np.dot(A,np.conj(inv.T))
            A=np.dot(A,inv)
            part2=np.real(np.trace(A))

            B = np.dot(B1,inv)
            B = scale*np.dot(B,np.diag(s))
            B = np.dot(B,A1)
            B = np.dot(B,np.diag(s0**2))
            part3=-2*np.real(np.trace(B))

            val.append(np.real(2/pi*scale*(part1+part2+part3)))
        
        np.savetxt(r.filename+'txt', val)

if r.is_real == -1:
    lam = np.logspace(r.lam0, r.lam1, r.lam_num)
    # svd to G21
    #G21real = funreal(funG21)
    #dG21real = funreal(funG21_dagger)
    U0,s0,V0 = rsvd(funG21, funG21_dagger, r.svd_tol, r.svd_maxit, shape)
    #U0,s0,V0 = rsvd(G21real, dG21real, r.svd_tol, r.svd_maxit, shape)
    s0=s0*(omega**2)
    
    # svd to G21G12
    #G2112real = funreal(funG21G12)
    #dG2112real = funreal(funG21G12_dagger)
    U,s,V = rsvd(funG21G12, funG21G12_dagger, r.svd_tol, r.svd_maxit, shape)
    #U,s,V = rsvd(G2112real, dG2112real, r.svd_tol, r.svd_maxit, shape)
    s=s*(omega**2)**2

    # svd to Im(G1)
    fun1=funImG(region1)
    U1,s1,V1 = rsvd(fun1, fun1, r.svd_tol, r.svd_maxit, shape)
    s1=-1.0*s1*(omega**2)

    # svd to Im(G2)
    fun2=funImG(region2)
    U2,s2,V2 = rsvd(fun2, fun2, r.svd_tol, r.svd_maxit, shape)
    s2=-1.0*s2*(omega**2)    

    if comm.rank == 0:
        VU = vec_product(V,U)
        U0Uc = vec_product(U0, vec_conj(U))
        U0U = vec_product(U0, U)
        U0U2 = vec_product(U0, U2)
        VcU = vec_product(vec_conj(V),U)
        VcU0 = vec_product(vec_conj(V),U0)
        VU0 = vec_product(V,U0)
        VcU2 = vec_product(vec_conj(V),U2)
        V0U1 = vec_product(V0,U1)
        V1V0 = vec_product(V1,V0)        
        V2U = vec_product(V2,U)
        V2U0 = vec_product(V2, U0)

        S = np.diag(s)
        S0 = np.diag(s0)
        S1 = np.diag(s1)
        S2 = np.diag(s2)        

        part1 = sum(s0**2)
        print 'part1= ',part1
        
        # np.save('s',[S0,S1,S2])
        # np.save('vector',[U,V,U1,V1])

        val=[]
        for scale in lam:
            inv = np.linalg.inv(np.eye(len(s))+scale**2*np.dot(S,VU))

            ###  part 2
            # U0Uc inv* S VcU0 S0^2  -scale^2
            A=np.dot(U0Uc,np.conj(inv))
            A=np.dot(A,S)
            A=np.dot(A,VcU0)
            A=np.dot(A,S0**2)
            part21=np.real(np.trace(A))*-1.0*scale**2

            # U0U2 S2 V2U0 S0^2  -scale
            A=np.dot(U0U2,S2)
            A=np.dot(A,V2U0)
            A=np.dot(A,S0**2)
            part22=np.real(np.trace(A))*-1.0*scale*r.is_geometry

            # U0U inv S VU0 S0^2  -scale^2
            A=np.dot(U0U,inv)
            A=np.dot(A,S)
            A=np.dot(A,VU0)
            A=np.dot(A,S0**2)
            part23=np.real(np.trace(A))*-1.0*scale**2

            # V0U1 S1 V1V0 S0^2  -scale
            A=np.dot(V0U1,S1)
            A=np.dot(A,V1V0)
            A=np.dot(A,S0**2)
            part24=np.real(np.trace(A))*-1.0*scale*r.is_geometry

            ###  part 3
            # U0Uc inv* S VcU2 S2 V2U0 S0^2 scale^3
            A=np.dot(U0Uc,np.conj(inv))
            A=np.dot(A,S)
            A=np.dot(A,VcU2)
            A=np.dot(A,S2)
            A=np.dot(A,V2U0)
            A=np.dot(A,S0**2)
            part31=np.real(np.trace(A))*scale**3*r.is_geometry

            # U0Uc inv* S VcU inv S VU0 S0^2 scale^4
            A=np.dot(U0Uc,np.conj(inv))
            A=np.dot(A,S)
            A=np.dot(A,VcU)
            A=np.dot(A,inv)
            A=np.dot(A,S)
            A=np.dot(A,VU0)
            A=np.dot(A,S0**2)
            part32=np.real(np.trace(A))*scale**4

            # U0Uc inv* S VcU0 S0 V0U1 S1 V1V0 S0 scale^3
            A=np.dot(U0Uc,np.conj(inv))
            A=np.dot(A,S)
            A=np.dot(A,VcU0)
            A=np.dot(A,S0)
            A=np.dot(A,V0U1)
            A=np.dot(A,S1)
            A=np.dot(A,V1V0)
            A=np.dot(A,S0)
            part33=np.real(np.trace(A))*scale**3*r.is_geometry

            # U0U2 S2 V2U inv S VU0 S0^2  scale^3
            A=np.dot(U0U2,S2)
            A=np.dot(A,V2U)
            A=np.dot(A,inv)
            A=np.dot(A,S)
            A=np.dot(A,VU0)
            A=np.dot(A,S0**2)
            part34=np.real(np.trace(A))*scale**3*r.is_geometry

            # U0U2 S2 V2U0 S0 V0U1 S1 V1V0 S0  scale^2
            A=np.dot(U0U2,S2)
            A=np.dot(A,V2U0)
            A=np.dot(A,S0)
            A=np.dot(A,V0U1)
            A=np.dot(A,S1)
            A=np.dot(A,V1V0)
            A=np.dot(A,S0)
            part35=np.real(np.trace(A))*scale**2*r.is_geometry**2

            # U0U inv S VU0 S0 V0U1 S1 V1V0 S0  scale^3
            A=np.dot(U0U,inv)
            A=np.dot(A,S)
            A=np.dot(A,VU0)
            A=np.dot(A,S0)
            A=np.dot(A,V0U1)
            A=np.dot(A,S1)
            A=np.dot(A,V1V0)
            A=np.dot(A,S0)
            part36=np.real(np.trace(A))*scale**3*r.is_geometry

            ## part 5
            # U0Uc inv* S VcU2 S2 V2U inv S VU0 S0 V0U1 S1 V1V0 S0 scale^6
            A=np.dot(U0Uc,np.conj(inv))
            A=np.dot(A,S)
            A=np.dot(A,VcU2)
            A=np.dot(A,S2)
            A=np.dot(A,V2U)
            A=np.dot(A,inv)
            A=np.dot(A,S)
            A=np.dot(A,VU0)
            A=np.dot(A,S0)
            A=np.dot(A,V0U1)
            A=np.dot(A,S1)
            A=np.dot(A,V1V0)
            A=np.dot(A,S0)
            part5=np.real(np.trace(A))*scale**6*r.is_geometry**2

            ## part 4
            # U0U2 S2 V2U inv S VU0 S0 V0U1 S1 V1V0 S0   -scale^4
            A=np.dot(U0U2,S2)
            A=np.dot(A,V2U)
            A=np.dot(A,inv)
            A=np.dot(A,S)
            A=np.dot(A,VU0)
            A=np.dot(A,S0)
            A=np.dot(A,V0U1)
            A=np.dot(A,S1)
            A=np.dot(A,V1V0)
            A=np.dot(A,S0)
            part41=np.real(np.trace(A))*-1.0*scale**4*r.is_geometry**2

            # U0Uc inv* S VcU inv S VU0 S0 V0U1 S1 V1V0 S0   -scale^5
            A=np.dot(U0Uc,np.conj(inv))
            A=np.dot(A,S)
            A=np.dot(A,VcU)
            A=np.dot(A,inv)
            A=np.dot(A,S)
            A=np.dot(A,VU0)
            A=np.dot(A,S0)
            A=np.dot(A,V0U1)
            A=np.dot(A,S1)
            A=np.dot(A,V1V0)
            A=np.dot(A,S0)
            part42=np.real(np.trace(A))*-1.0*scale**5*r.is_geometry

            # U0Uc inv* S VcU2 S2 V2U0 S0 V0U1 S1 V1V0 S0  -scale^4
            A=np.dot(U0Uc,np.conj(inv))
            A=np.dot(A,S)
            A=np.dot(A,VcU2)
            A=np.dot(A,S2)
            A=np.dot(A,V2U0)
            A=np.dot(A,S0)
            A=np.dot(A,V0U1)
            A=np.dot(A,S1)
            A=np.dot(A,V1V0)
            A=np.dot(A,S0)
            part43=np.real(np.trace(A))*-1.0*scale**4*r.is_geometry**2

            # U0Uc inv* S VcU2 S2 V2U inv S VU0 S0^2  -scale^5
            A=np.dot(U0Uc,np.conj(inv))
            A=np.dot(A,S)
            A=np.dot(A,VcU2)
            A=np.dot(A,S2)
            A=np.dot(A,V2U)
            A=np.dot(A,inv)
            A=np.dot(A,S)
            A=np.dot(A,VU0)
            A=np.dot(A,S0**2)
            part44=np.real(np.trace(A))*-1.0*scale**5*r.is_geometry

            part = part1+part21+part22+part23+part24+part31+part32+part33+part34+part35+part36+part5+part41+part42+part43+part44
            val.append(np.real(2/pi*scale**2*part))

        np.savetxt(r.filename+'txt', val)

