function phi = compute_RHTbound(filename,x0)
% COMPUTE_RHTBOUND - 

sigma0 = load(filename);
sigma=sigma0.^2;
xmin=1/sigma(1);
xmax=1/sigma(end);
fprintf('xmin=%1.2e, xmax=%1.2e\n',xmin,xmax)

phi=2/pi*sum(x0*sigma./(1+x0*sigma).^2);
