pre = 'Mx150-My200-w0.3-Mz8-f1.987-n1-N7-tol1e-4-g0.5-w1.3-p0.2-m4-R16-s0';

file = strcat(pre,'.x0.h5');
epsfile = strcat(pre,'.epsF.h5');
hx=0.02;
freq=1.987;
PML=10;
pextend=5;

Mx0=36;
My0=101;
Mz0=36;
Mx=150;
My=200;
Mz=8;
wx0=103;
wtx=15;
wz0=36;
wtz=8;
extend=4;

Ex=h5read(file,'/xr')+1i*h5read(file,'/xi');
Ey=h5read(file,'/yr')+1i*h5read(file,'/yi');
Ez=h5read(file,'/zr')+1i*h5read(file,'/zi');

ep=h5read(epsfile,'/data');
[Nz,Ny,Nx]=size(Ex);
%----curlE  center-difference
% curlEx=dy Ez-dz Ey
curlEx=zeros(size(Ex));
curlEx(:,2:end-1,:)=(Ez(:,3:end,:)-Ez(:,1:end-2,:))/2/hx;
curlEx(2:end-1,:,:)=curlEx(2:end-1,:,:)-(Ey(3:end,:,:)-Ey(1:end-2,:,:))/2/hx;
% curlEy=dz Ex-dx Ez
curlEy=zeros(size(Ex));
curlEy(2:end-1,:,:)=(Ex(3:end,:,:)-Ex(1:end-2,:,:))/2/hx;
curlEy(:,:,2:end-1)=curlEy(:,:,2:end-1)-(Ez(:,:,3:end)-Ez(:,:,1:end-2))/2/hx;
% curlEz=dx Ey-dy Ex
curlEz=zeros(size(Ex));
curlEz(:,:,2:end-1)=(Ey(:,:,3:end)-Ey(:,:,1:end-2))/2/hx;
curlEz(:,2:end-1,:)=curlEz(:,2:end-1,:)-(Ex(:,3:end,:)-Ex(:,1:end-2,:))/2/hx;
%---- H=i curlE/omega
Hx=1i*curlEx/(freq*2*pi);
Hy=1i*curlEy/(freq*2*pi);
Hz=1i*curlEz/(freq*2*pi);

%---- Poynting flux S=1/2*Real(E* cross H)
Sx=0.5*real(conj(Ey).*Hz-conj(Ez).*Hy);
Sy=0.5*real(conj(Ez).*Hx-conj(Ex).*Hz);
Sz=0.5*real(conj(Ex).*Hy-conj(Ey).*Hx);

p0=15;
subplot(2,3,1)
m=Sx(:,:,p0);m=reshape(m,Nz,Ny);pcolor(m);shading flat;colorbar;
title('Sx (lower)')
Px1=sum(sum(m(PML+pextend:Nz-PML-pextend,PML+pextend:Ny-PML-pextend)))*hx^2;

subplot(2,3,2)
m=Sy(:,p0,:);m=reshape(m,Nz,Nx);pcolor(m);shading flat;colorbar;
title('Sy (lower)')
Py1=sum(sum(m(PML+pextend:Nz-PML-pextend,PML+pextend:Nx-PML-pextend)))*hx^2;

subplot(2,3,3)
m=Sz(p0,:,:);m=reshape(m,Ny,Nx);pcolor(m);shading flat;colorbar;
title('Sz (lower)')
Pz1=sum(sum(m(PML+pextend:Ny-PML-pextend,PML+pextend:Nx-PML-pextend)))*hx^2;

subplot(2,3,4)
m=Sx(:,:,Nx-p0);m=reshape(m,Nz,Ny);pcolor(m);shading flat;colorbar;
title('Sx (higher)')
Px2=sum(sum(m(PML+pextend:Nz-PML-pextend,PML+pextend:Ny-PML-pextend)))*hx^2;

subplot(2,3,5)
m=Sy(:,Ny-p0,:);m=reshape(m,Nz,Nx);pcolor(m);shading flat;colorbar;
title('Sy (higher)')
Py2=sum(sum(m(PML+pextend:Nz-PML-pextend,PML+pextend:Nx-PML-pextend)))*hx^2;

subplot(2,3,6)
m=Sz(Nz-p0,:,:);m=reshape(m,Ny,Nx);pcolor(m);shading flat;colorbar;
title('Sz (higher)')
Pz2=sum(sum(m(PML+pextend:Ny-PML-pextend,PML+pextend:Nx-PML-pextend)))*hx^2;

%wvg
m=Sy(wz0-extend:wz0+wtz+extend,p0,wx0-extend:wx0+wtx+extend);
Pw=sum(m(:))*hx^2;
Pother=Px1+Py1+Pz1-Px2-Py2-Pz2-Pw;
%cavity energy = 0.25 * (ep*|E|^2+|H|^2)
epc=ep(Mz0-extend:Mz0+Mz+extend,My0-extend:My0+My+extend,Mx0-extend:Mx0+Mx+extend);
Exc=Ex(Mz0-extend:Mz0+Mz+extend,My0-extend:My0+My+extend,Mx0-extend:Mx0+Mx+extend);
Eyc=Ey(Mz0-extend:Mz0+Mz+extend,My0-extend:My0+My+extend,Mx0-extend:Mx0+Mx+extend);
Ezc=Ez(Mz0-extend:Mz0+Mz+extend,My0-extend:My0+My+extend,Mx0-extend:Mx0+Mx+extend);
Hxc=Hx(Mz0-extend:Mz0+Mz+extend,My0-extend:My0+My+extend,Mx0-extend:Mx0+Mx+extend);
Hyc=Hy(Mz0-extend:Mz0+Mz+extend,My0-extend:My0+My+extend,Mx0-extend:Mx0+Mx+extend);
Hzc=Hz(Mz0-extend:Mz0+Mz+extend,My0-extend:My0+My+extend,Mx0-extend:Mx0+Mx+extend);

UE=epc.*(abs(Exc).^2+abs(Eyc).^2+abs(Ezc).^2);
UH=(abs(Exc).^2+abs(Eyc).^2+abs(Ezc).^2);
%U=0.25*sum(UE(:)+UH(:))*hx^3;
U=0.5*sum(UE(:))*hx^3;
Q = 2*pi*freq*U/(abs(Pother)+abs(Pw));
fprintf('wvg flux = %g, radiative flux = %g, Qtotal = %g\n',abs(Pw),abs(Pother),Q)
