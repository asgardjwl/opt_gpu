E1x=h5read('t1.h5','/xr')+h5read('t1.h5','/xi')*1i;
E1y=h5read('t1.h5','/yr')+h5read('t1.h5','/yi')*1i;
E1z=h5read('t1.h5','/zr')+h5read('t1.h5','/zi')*1i;

E2x=h5read('t2.h5','/xr')+h5read('t2.h5','/xi')*1i;
E2y=h5read('t2.h5','/yr')+h5read('t2.h5','/yi')*1i;
E2z=h5read('t2.h5','/zr')+h5read('t2.h5','/zi')*1i;

n=norm(E1x(:))^2+norm(E1y(:))^2+norm(E1z(:))^2;
d=norm(E1x(:)-E2x(:))^2+norm(E1y(:)-E2y(:))^2+norm(E1z(:)-E2z(:))^2;

d/n
subplot(1,2,1)
E1=abs(E1x).^2+abs(E1y).^2+abs(E1z).^2;
m=squeeze(E1(1,:,:));pcolor(real(m));shading flat;colorbar;axis tight equal

subplot(1,2,2)
E2=abs(E2x).^2+abs(E2y).^2+abs(E2z).^2;
m=squeeze(E2(1,:,:));pcolor(real(m));shading flat;colorbar;axis tight equal
