R=0.15;
d0=0.1;
d=d0+2*R;
k=2*pi;
ep=2+.1*1i;

x=k*R;
y=sqrt(ep)*x;
alpha = 2*pi*R^3*(2*(sin(y)-y*cos(y))-x^2*(-sin(y)/y^2+cos(y)/y+sin(y)))/((sin(y)-y*cos(y))+x^2*(-sin(y)/y^2+cos(y)/y+sin(y)));
alpha=4*pi*R^3*(ep-1)/(ep+2);

phi=imag(alpha)^2*k^6*(3/(k*d)^6+1/(k*d)^4+1/(k*d)^2)/4/pi^3

V=4/3*pi*R^3;
x0=(abs(ep-1)^2/imag(ep))^2;
y_sphere=3*2/pi*x0*V^2/8/pi^2/d0^6
y_dipole=2/pi*x0*V^2/8/pi^2/d0^6.*((1+x0*V^2/16/pi^2/d0^6).^-2+2*(1+x0*V^2/4/pi^2/d0^6).^-2)