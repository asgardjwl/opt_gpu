pre1 = 'Mx150-My200-w0.3-Mz8-f1-n1-N7-tol1e-4-g0.5-w1.3-p0.2-m4-R16-s0';
pre2 = 'Mx150-My200-w0.3-Mz8-f1.987-n1-N7-tol1e-4-g0.5-w1.3-p0.2-m4-R16-s0';

f1file = strcat(pre1,'.x0.h5');
eps1file = strcat(pre1,'.epsF.h5');
f2file = strcat(pre2,'.x0.h5');
eps2file = strcat(pre2,'.epsF.h5');
hxyz=0.02^3;
chi=100e-12;
e0=8.8541878e-12;
lam=1.55e-6;
Q1=1000;
Q2=590;
Q1r=2000;
Q2r=590*2;

Mx0=36;
My0=101;
Mz0=36;
Mx=150;
My=200;
Mz=8;

Ex1=h5read(f1file,'/xr')+1i*h5read(f1file,'/xi');
Ex1c=Ex1(Mz0:Mz0+Mz-1,My0:My0+My-1,Mx0:Mx0+Mx-1);
Ey1=h5read(f1file,'/yr')+1i*h5read(f1file,'/yi');
Ey1c=Ey1(Mz0:Mz0+Mz-1,My0:My0+My-1,Mx0:Mx0+Mx-1);
Ez1=h5read(f1file,'/zr')+1i*h5read(f1file,'/zi');
Ez1c=Ez1(Mz0:Mz0+Mz-1,My0:My0+My-1,Mx0:Mx0+Mx-1);

Ex2=h5read(f2file,'/xr')+1i*h5read(f2file,'/xi');
Ex2c=Ex2(Mz0:Mz0+Mz-1,My0:My0+My-1,Mx0:Mx0+Mx-1);
Ey2=h5read(f2file,'/yr')+1i*h5read(f2file,'/yi');
Ey2c=Ey2(Mz0:Mz0+Mz-1,My0:My0+My-1,Mx0:Mx0+Mx-1);
Ez2=h5read(f2file,'/zr')+1i*h5read(f2file,'/zi');
Ez2c=Ez2(Mz0:Mz0+Mz-1,My0:My0+My-1,Mx0:Mx0+Mx-1);

eps1=h5read(eps1file,'/data');
eps1c=eps1(Mz0:Mz0+Mz-1,My0:My0+My-1,Mx0:Mx0+Mx-1);
eps2=h5read(eps2file,'/data');
eps2c=eps2(Mz0:Mz0+Mz-1,My0:My0+My-1,Mx0:Mx0+Mx-1);
epsall=eps2c;epsall(epsall<5)=0;epsall(epsall>0)=1;

norm1=sqrt(sum(sum(sum((Ex1c.*conj(Ex1c)+Ey1c.*conj(Ey1c)+Ez1c.*conj(Ez1c)).*eps1c*hxyz))));
norm2=sqrt(sum(sum(sum((Ex2c.*conj(Ex2c)+Ey2c.*conj(Ey2c)+Ez2c.*conj(Ez2c)).*eps2c*hxyz))));
lap=abs(sum(sum(sum(Ex1c.*Ex1c.*conj(Ez2c).*epsall*hxyz))));
beta=lap/norm1/norm1/norm2

Ex1p=Ex1c/sqrt(2)+Ey1c/sqrt(2);
Ey1p=Ey1c/sqrt(2)-Ex1c/sqrt(2);
lap=abs(sum(sum(sum(Ex1p.*Ex1p.*conj(Ez2c).*epsall*hxyz))));
beta1=lap/norm1/norm1/norm2

beta1_u=beta1*chi/sqrt(e0*lam^3);
eta=128/(2*pi*3e8/lam)*(beta1_u/2)^2*Q1^2*Q2*(1-Q1/Q1r)^2*(1-Q2/Q2r)