import h5py
import numpy as np
from mpi4py.MPI import COMM_WORLD as comm
import time, sys, argparse
from math import pi

from lib.tool import write_results, PML, interp, gradient_check
from lib.gce.grid import Grid
from lib.gce import verbose
from lib.gce.space import initialize_space
from lib.solver import Solver
from lib.filter import Filter3d
from lib.RHT import RHT_box_gradient

sys.path.append('/home/weiliang/nlopt-2.5.0/lib/python2.7/site-packages/')
import nlopt

dtype=np.complex128
#------------- passing argument---------------#
parser = argparse.ArgumentParser()
# solvers
parser.add_argument('-name', action="store", type=str, default='test')
parser.add_argument('-maxit', action="store", type=int, default=100000)
parser.add_argument('-tol', action="store", type=float, default=1e-6)
parser.add_argument('-verbose', action="store", type=int, default=1)
parser.add_argument('-solverbase', action="store", type=int, default=100)
parser.add_argument('-init_type', action="store", type=str, default='zero')
# svd
parser.add_argument('-svd_maxit', action="store", type=int, default=50)
parser.add_argument('-svd_tol', action="store", type=float, default=1e-2)
# filter
parser.add_argument('-bproj', action="store", type=float, default=0.0)
parser.add_argument('-R', action="store", type=int, default=0)
parser.add_argument('-alpha', action="store", type=float, default=1.0)
# Job
parser.add_argument('-Job', action="store", type=int, default=1)
parser.add_argument('-outputbase', action="store", type=int, default=2)
parser.add_argument('-export_in_object', action="store", type=int, default=1)
parser.add_argument('-maxeval', action="store", type=int, default=5000)
parser.add_argument('-alg', action="store", type=int, default=24)
# temporal
parser.add_argument('-freq', action="store", type=float, default=1.0)
parser.add_argument('-Qabs', action="store", type=float, default=1e16)
# geometry
parser.add_argument('-Nx', action="store", type=int, default=75)
parser.add_argument('-Ny', action="store", type=int, default=75)
parser.add_argument('-Nz', action="store", type=int, default=70)

parser.add_argument('-Mzslab', action="store", type=int, default=0)
parser.add_argument('-rep', action="store", type=int, default=1)
parser.add_argument('-Mx1', action="store", type=int, default=100)
parser.add_argument('-My1', action="store", type=int, default=100)
parser.add_argument('-Mz1', action="store", type=int, default=0)
parser.add_argument('-dx1', action="store", type=int, default=2)
parser.add_argument('-dy1', action="store", type=int, default=2)
parser.add_argument('-dz1', action="store", type=int, default=1)

parser.add_argument('-Mx2', action="store", type=int, default=100)
parser.add_argument('-My2', action="store", type=int, default=104)
parser.add_argument('-Mz2', action="store", type=int, default=0)
parser.add_argument('-dx2', action="store", type=int, default=2)
parser.add_argument('-dy2', action="store", type=int, default=2)
parser.add_argument('-dz2', action="store", type=int, default=1)

parser.add_argument('-Npmlx', action="store", type=int, default=10)
parser.add_argument('-Npmly', action="store", type=int, default=10)
parser.add_argument('-Npmlz', action="store", type=int, default=10)

parser.add_argument('-hx', action="store", type=float, default=0.02)
parser.add_argument('-hy', action="store", type=float, default=0.02)
parser.add_argument('-hz', action="store", type=float, default=0.02)
# dielectric
parser.add_argument('-epsbkg', action="store", type=float, default=1.0)
parser.add_argument('-epsdiff', action="store", type=float, default=11.0)
parser.add_argument('-epsimag', action="store", type=float, default=1.0)
parser.add_argument('-epsfile', action="store", type=str, default='init.txt')
parser.add_argument('-epstype', action="store", type=str, default='vac')

r, unknown = parser.parse_known_args(sys.argv[1:])
if comm.rank==0:
    for arg in vars(r):
        print arg," is ",getattr(r,arg)
        
verbose.v=r.verbose
verbose.solverbase=r.solverbase
verbose.filename=r.name
verbose.outputbase=r.outputbase
verbose.export_in_object=r.export_in_object
verbose.init_type=r.init_type
# ----------assembling parameters------------#
if r.Qabs>1e10:
    r.Qabs=float('inf')
    
omega = dtype(r.freq*2*pi) - 1j*dtype(r.freq*2*pi/2/r.Qabs)
shape = (r.Nx,r.Ny,r.Nz)
# pml
pmlx=PML(r.Npmlx,omega,r.Nx,r.hx)
pmly=PML(r.Npmly,omega,r.Ny,r.hy)
pmlz=PML(r.Npmlz,omega,r.Nz,r.hz)
pml_p=[pmlx.sp*r.hx,pmly.sp*r.hy,pmlz.sp*r.hz]
pml_d=[pmlx.sd*r.hx,pmly.sd*r.hy,pmlz.sd*r.hz]

#interp matrix
Aup = interp(r.dx1,r.dy1,r.dz1,r.Mzslab,r.Nx,r.Ny,r.Nz,r.Mx1,r.My1,r.Mz1,r.rep)
Adown = interp(r.dx2,r.dy2,r.dz2,r.Mzslab,r.Nx,r.Ny,r.Nz,r.Mx2,r.My2,r.Mz2,r.rep)
ndof1 = Aup.Mx/Aup.rep*Aup.My/Aup.rep*Aup.Mz/Aup.rep
ndof2 = Adown.Mx/Adown.rep*Adown.My/Adown.rep*Adown.Mz/Adown.rep
ndof=ndof1+ndof2

if r.epstype == 'one':
    dof=np.ones(ndof).astype(np.float)
elif r.epstype == 'rand':
    dof=np.random.random(ndof).astype(np.float)
elif r.epstype == 'file':
    f = open(r.epsfile, 'r')
    dof = np.loadtxt(f)
elif r.epstype == 'vac':
    dof=np.zeros(ndof).astype(np.float)
    dof[ndof1-1]=1.0
    dof[ndof1]=1.0

if comm.rank == 0:
    print "The size of the problem is",shape
    # epsilon profile
    epsBkg = np.ones(shape)*r.epsbkg
    ep = dtype(np.copy(epsBkg))
    doftmp1 = dof[0:ndof1].reshape(Aup.Mx/Aup.rep,Aup.My/Aup.rep,Aup.Mz/Aup.rep)
    doftmp2 = dof[ndof1:].reshape(Adown.Mx/Adown.rep,Adown.My/Adown.rep,Adown.Mz/Adown.rep)
    Aup.matA(doftmp1,ep,r.epsdiff-1j*r.epsimag)
    Adown.matA(doftmp2,ep,r.epsdiff-1j*r.epsimag)
    e = [ep, ep, ep]
    write_results('epsF.h5',e)
else:
    epsBkg=None

#-------opertors----------#
#initiliaze GPU
initialize_space(shape)
# solver
solver = Solver(shape, r.tol, r.maxit, pml_p, pml_d, omega)
FT = Filter3d(r.bproj,r.R,r.alpha)
#-------solve-----------#
grad=np.zeros(ndof)
RHT_old=RHT_box_gradient(shape, epsBkg, r.epsdiff, r.epsimag, solver, Aup, Adown, r.svd_tol, r.svd_maxit)
RHT_nlopt = FT.new_object(RHT_old)

if r.Job is 0:
    grad=np.array([])
    val=RHT_nlopt(dof.flatten(),grad)
    print val
    
elif r.Job is -1:
    verbose.count=1
    gradient_check(RHT_nlopt, dof.flatten(), grad, index=1, Ntotal=20, start=0.0, dx=1e-2)
    
elif r.Job is 1:
    lb=np.ones(ndof)*0.0
    ub=np.ones(ndof)*1.0

    if r.alg == 24:
        opt = nlopt.opt(nlopt.LD_MMA, ndof)
    elif r.alg == 11:
        opt = nlopt.opt(nlopt.LD_LBFGS, ndof)
    opt.set_lower_bounds(lb)
    opt.set_upper_bounds(ub)
    
    opt.set_maxeval(r.maxeval)

    opt.set_max_objective(RHT_nlopt)
    
    x = opt.optimize(dof.flatten())
    minf = opt.last_optimum_value()

