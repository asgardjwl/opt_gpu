import h5py
import numpy as np
import time, sys, argparse
from math import pi

from lib.tool import interp

sys.path.append('/home/weiliang/nlopt-2.5.0/lib/python2.7/site-packages/')
import nlopt
dtype=np.float64
#------------- passing argument---------------#
parser = argparse.ArgumentParser()
parser.add_argument('-Mx', action="store", type=int, default=100)
parser.add_argument('-My', action="store", type=int, default=120)
parser.add_argument('-rep', action="store", type=int, default=2)
parser.add_argument('-maxeval', action="store", type=int, default=5000)
parser.add_argument('-epsfile', action="store", type=str, default='c.txt')
parser.add_argument('-epstype', action="store", type=str, default='vac')
parser.add_argument('-eps_fit', action="store", type=str, default='b.txt')


r, unknown = parser.parse_known_args(sys.argv[1:])
for arg in vars(r):
    print arg," is ",getattr(r,arg)
        
# ----------assembling parameters------------#
#interp matrix
ndof=(r.Mx/r.rep)*(r.My/r.rep)
A = interp(r.Mx,r.My,1,1,r.Mx,r.My,1,0,0,0,r.rep)

if r.epstype == 'one':
    dof=np.ones([r.Mx/r.rep,r.My/r.rep]).astype(np.float)
elif r.epstype == 'rand':
    dof=np.random.randn(r.Mx/r.rep,r.My/r.rep).astype(np.float)
elif r.epstype == 'file':
    f = open(r.epsfile, 'r')
    dof = np.loadtxt(f).reshape(r.Mx/r.rep,r.My/r.rep)
elif r.epstype == 'vac':
    dof=np.zeros([r.Mx/r.rep,r.My/r.rep]).astype(np.float)

f = open(r.eps_fit, 'r')
epsfit = np.loadtxt(f).reshape(r.Mx,r.My,1)    

def fun(dof,grad):
    doftmp = dof.reshape(A.Mx/A.rep,A.My/A.rep)
    ep = dtype(np.zeros((r.Mx,r.My,1)))
    A.matA(doftmp,ep,1.0)
    de = ep - epsfit

    val = np.vdot(de,de)

    if grad.size>0:
        gradtmp=dtype(np.zeros((A.Mx/A.rep,A.My/A.rep)))
        g = [de,dtype(np.zeros((r.Mx,r.My,1))),dtype(np.zeros((r.Mx,r.My,1)))]
        A.matAT(gradtmp,g)

        grad[:]=2*gradtmp.flatten()
        
    return val

# before optimization
grad=np.array([])    
val0 = fun(dof,grad)
print "Before optimization, the error is ", val0

# optimization
lb=np.ones(ndof)*0.0
ub=np.ones(ndof)*1.0
    
opt = nlopt.opt(nlopt.LD_MMA, ndof)
opt.set_lower_bounds(lb)
opt.set_upper_bounds(ub)
    
opt.set_maxeval(r.maxeval)

opt.set_min_objective(fun)
    
x = opt.optimize(dof.flatten())
minf = opt.last_optimum_value()

np.savetxt('dof.txt', x)
print "After optimization, the error is ", minf
