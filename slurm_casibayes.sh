#!/bin/bash
#SBATCH --job-name=dchi
#SBATCH --output=test.txt
#SBATCH --gres=gpu:1
#SBATCH -N 1
#SBATCH --ntasks-per-node=1
#SBATCH --time=01:00:00
#SBATCH --mem=8000
#SBATCH --error=test.err
##SBATCH --mail-type=begin
##SBATCH --mail-type=end
#SBATCH --mail-user=wljin@stanford.edu

module load anaconda/5.3.0 openmpi/gcc/2.1.0/64 cudatoolkit/8.0
#module load anaconda/5.3.0 openmpi/cuda-8.0/gcc/2.1.0/64 cudatoolkit/9.0

prop=casimirbayes.py
np=1

Ntotal=4
Qabs=1e16
hx=0.05
hy=$hx
hz=$hx
stypexy=-1
stypez=-1

d=0.5
gap=0.5
gap_dipole=1
mx=2
my=2
mz=2
Rmin=$hx
Rmax=$mx
centeroff=1
outputbase=5

epsdiff=10
alphax=0
alphay=0
alphaz=1

fstart=0.1
fend=1.
fN=1

maxeval=200
relearn=10
inner=20
sample=10
hole=1

Nx=$(echo "($mx+2*$gap)/$hx" | bc);
Ny=$(echo "($my+2*$gap)/$hy" | bc);
Nz=$(echo "($mz+$d+$gap+$gap_dipole)/$hz" | bc);

Mx=$(echo "$mx/$hx" | bc);
My=$(echo "$my/$hy" | bc);
Mz=$(echo "$mz/$hz" | bc);

Mx0=$((($Nx-$Mx)/2))
My0=$((($Ny-$My)/2))
Mz0=$(echo "$gap/$hz" | bc);

Npmlx=10
Npmly=10
Npmlz=10

epsbkg=1
cx=$(($Nx/2))
cy=$(($Ny/2))
cz=$(echo "($gap+$mz+$d)/$hz" | bc);

fpre='casimir-s$stypexy-$stypez-$alphax$alphay$alphaz-d$d-Mx$Mx-My$My-Mz$Mz-hx$hx-chi$epsdiff-fS$fstart-fe$fend-fN$fN'
name=`hostname`
ID=$$
outputfile=$name-$fpre.$ID-out

# solver
maxit=100000
tol=1e-6
verbose=1
init=5
init_type='rand'
solverbase=5000

eval "mpiexec -n $np python $prop -Nx $Nx -Ny $Ny -Nz $Nz -Mx $Mx -My $My -Mz $Mz -Mx0 $Mx0 -My0 $My0 -Mz0 $Mz0 -hx $hx -hy $hy -hz $hz -Npmlx $Npmlx -Npmly $Npmly -Npmlz $Npmlz -epsbkg $epsbkg -epsdiff $epsdiff -epstype $epstype -epsfile $epsfile -cx $cx -cy $cy -cz $cz -maxit $maxit -tol $tol -verbose $verbose -solverbase $solverbase -init $init -init_type $init_type -ifreq_start $fstart -ifreq_end $fend -ifreq_N $fN -alphax $alphax -alphay $alphay -alphaz $alphaz -outputbase $outputbase -name $fpre -Qabs $Qabs -stypexy $stypexy -stypez $stypez -maxeval $maxeval -relearn $relearn -inner $inner -sample $sample -hole $hole -Ntotal $Ntotal -Rmin $Rmin -Rmax $Rmax -centeroff $centeroff >$outputfile"
