#!/bin/bash
#SBATCH --job-name=test
#SBATCH --output=grad.txt
#SBATCH --gres=gpu:1
#SBATCH -N 1
#SBATCH --ntasks-per-node=1
#SBATCH --time=01:00:00
#SBATCH --mem=14000
#SBATCH --error=test.err
##SBATCH --mail-type=begin
##SBATCH --mail-type=end
#SBATCH --mail-user=weiliang@princeton.edu

module load anaconda/5.3.0 openmpi/gcc/2.1.0/64 cudatoolkit/8.0
#module load anaconda/5.3.0 openmpi/cuda-8.0/gcc/2.1.0/64 cudatoolkit/9.0

prop=fdfd_casimir.py
np=1

hx=0.05
hy=$hx
hz=$hx

Nx=100
Ny=100
Nz=100

Mx=10
My=10
Mz=10

Mx0=45
My0=45
Mz0=20
Mzslab=0

Npmlx=10
Npmly=10
Npmlz=10

epsbkg=1
epsdiff=10
epstype='rand'
epsfile='c.txt'

Jamp=1.0
Jdir=2
cx=$(($Nx/2))
cy=$(($Ny/2))
cz=35

# solver
maxit=100000
tol=1e-8
verbose=1
init=2
init_type='rand'
solverbase=5000

mpiexec -n $np python $prop -Nx $Nx -Ny $Ny -Nz $Nz -Mx $Mx -My $My -Mz $Mz -Mx0 $Mx0 -My0 $My0 -Mz0 $Mz0 -Mzslab $Mzslab -hx $hx -hy $hy -hz $hz -Npmlx $Npmlx -Npmly $Npmly -Npmlz $Npmlz -epsbkg $epsbkg -epsdiff $epsdiff -epstype $epstype -epsfile $epsfile -Jamp $Jamp -Jdir $Jdir -cx $cx -cy $cy -cz $cz -maxit $maxit -tol $tol -verbose $verbose -solverbase $solverbase -init $init -init_type $init_type
