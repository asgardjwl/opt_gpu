# solver info
v=2;  #verbose = 1, 2
solverbase=100   # print solver info every such iterations

# solver routine
init=10   # for first such solves, starting from particular value
init_type='zero' # with value being this

# optimization info
count=0
outputbase=2
export_in_object=1 # export epsopt during optimization 
filename='tmp'

# others
outputfield=0
phase=0.0
lap=0.0
lap_x0=0
lap_y0=0
lap_z0=0
lap_tx=0
lap_ty=0
lap_tz=0
