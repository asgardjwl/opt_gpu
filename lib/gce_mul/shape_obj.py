from mpi4py.MPI import COMM_WORLD as comm

class shape_obj():
    def __init__(self,shape):
        # Make sure shape has exactly three elements.
        if len(shape) is not 3:
            raise TypeError('Shape must have exactly three elements.')
        # Make sure they are all integers.
        if any([type(s) is not int for s in shape]):
            raise TypeError('Shape must have only integer elements.')
        # Make sure all elements are positive.
        if any([s < 1 for s in shape]):
            raise TypeError('Shape must have only integer elements.')
        # Initialize the shape.
        self.shape = shape

        size = comm.Get_size()
        rank = comm.Get_rank() # Find out which process I am.
        get_x_range = lambda r: (int(self.shape[0] * (float(r) / size)), \
                                int(self.shape[0] * (float(r+1) / size)))
        
        self.x_range = get_x_range(rank)
        self.all_x_ranges = [get_x_range(r) for r in range(size)]        
